#!pip install numpy==1.23.0 keras==2.13.1 tensorflow==2.13.1 tensorflow_addons==0.21.0 tfp-nightly==0.22.0.dev20231002 h5py 
#!pip install -U scikit-learn

import gc
from pylab import *
import matplotlib
matplotlib.use('agg')
import random
import re
import h5py 
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import MinMaxScaler
import tensorflow as tf
import tensorflow_addons as tfa
import tensorflow_probability as tfp
from tensorflow.keras import backend as K
import numpy as np


tfd = tfp.distributions
tfpl = tfp.layers


#Functions used for NN training 
def prior(kernel_size, bias_size=0, dtype=None):
  n = kernel_size + bias_size
  return tf.keras.Sequential([
      tfp.layers.VariableLayer(n, dtype=dtype),
      tfp.layers.DistributionLambda(lambda t: tfd.Independent(
          tfd.Normal(loc=t, scale=1),
          reinterpreted_batch_ndims=1))])

def posterior(kernel_size, bias_size=0, dtype=None):
  n = kernel_size + bias_size
  c = np.log(np.expm1(1.))
  return tf.keras.Sequential([
      tfp.layers.VariableLayer(2 * n, dtype=dtype),
      tfp.layers.DistributionLambda(lambda t: tfd.Independent(
          tfd.Normal(loc=t[..., :n],
                     scale=1e-5 + 0.0015 * tf.nn.softplus(c + t[..., n:])),
          reinterpreted_batch_ndims=1))])

def custom_loss(targets, estimated_distribution):
  return -estimated_distribution.log_prob(targets)

#Function to perform bayesian NN training. 
#inputs is 2d array (nsamples,nfeatures)
#uutputs is 1d array (nsamples)
#scale_fname is where the scaling terms are stored 
#model_fname is where the NN model is stored 
#NN parameters such as batch size, epochs, initial learning rate, max learning rate are set to defaults but can be adjusted 
#If plot_loss is True, then a plot is made showing the training/validation loss for given loss_fname 
def train_nn(inputs,outputs,scale_fname='TestScale',model_fname='TestModel',batch_size=128,epochs=300,init_lr=1e-4,max_lr=1e-2,test_size=0.7,loss_fname='Loss.png',plot_loss=False):
    sc_in = MinMaxScaler(feature_range=(0,1))
    inputs = sc_in.fit_transform(inputs)
    gc.collect()

    sc_out = MinMaxScaler(feature_range=(0,1))
    outputs = outputs.reshape(-1,1)
    outputs = sc_out.fit_transform(outputs)

    input_scale = sc_in.scale_
    input_min = sc_in.data_min_

    output_scale = sc_out.scale_
    output_min = sc_out.data_min_
    
    f = h5py.File(scale_fname,'w')
    f.create_dataset('Input_Scale',data=input_scale)
    f.create_dataset('Input_Min',data=input_min)
    f.create_dataset('Output_Scale',data=output_scale)
    f.create_dataset('Output_Min',data=output_min)
    f.close()

    inputs_train,inputs_test,outputs_train,outputs_test = train_test_split(inputs,outputs,test_size=test_size)

    del inputs_test, outputs_test
    gc.collect()

    n_samples, n_features = inputs_train.shape

    nn_inp = tf.keras.Input(shape=(n_features))


    out = tfp.layers.DenseVariational(128, posterior, prior, kl_weight=1/(n_samples/batch_size), activation='relu')(nn_inp)
    out = tfp.layers.DenseVariational(128, posterior, prior, kl_weight=1/(n_samples/batch_size), activation='relu')(out)
    out = tfp.layers.DenseVariational(64, posterior, prior, kl_weight=1/(n_samples/batch_size), activation='relu')(out)
    
    out  = tf.keras.layers.Dense(units=2)(out)
    out = tfp.layers.IndependentNormal(1)(out)

    callback = tf.keras.callbacks.EarlyStopping(monitor='loss', patience=20)

    #Reverting to constant learning rate, the cyclical learning rate tends to be unstable for densevariational layers. The model appears to be training as 
    #expected when using the cyclical learning rate but in the end the results seem to not be correct for some reason. Using a constant learning rate improves 
    #this issue. Need to investigate this issue further. 
    #steps_per_epoch = n_samples // batch_size
    #clr = tfa.optimizers.CyclicalLearningRate(initial_learning_rate=init_lr,maximal_learning_rate=max_lr,scale_fn=lambda z: 1/(2.**(z-1)),step_size=2.*(n_samples/batch_size))
    #opt = tf.keras.optimizers.Adam(learning_rate=clr)

    opt = tf.optimizers.Adam(init_lr)

    model = tf.keras.Model(nn_inp, out)
    model.compile(loss=custom_loss, optimizer=opt, metrics=['mae','mse','acc','mape'])                                                                                                                                                     
    history = model.fit(inputs_train, outputs_train, epochs=epochs, batch_size=batch_size,validation_split=0.5)
    model.save(model_fname)

    if plot_loss:
        plt.plot(history.history['loss'],color='b')
        plt.plot(history.history['val_loss'],color='r')
        plt.savefig(loss_fname)
        plt.clf()
        plt.close()


    tf.keras.backend.clear_session()

#Code to run trained NN and make predictions
def run_nn(inputs,input_min,input_scale,output_min,output_scale,model_fname='TestModel'):
        for i in range(len(inputs[0,:])):
            inputs[:,i] = (inputs[:,i] - input_min[i])*input_scale[i]
    
        x,y = inputs.shape
        output = np.zeros((100,x))+np.nan

        model =  tf.keras.models.load_model(model_fname,compile=False)
             
        for i in range(100):
            output[i,:] = model(inputs.reshape(x,y))[:,0].numpy()
    
        output = np.nanmean(output,axis=0)
        return (output.flatten()/output_scale[0]) + output_min[0]



